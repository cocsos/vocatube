package me.cocsos.lineword.viewmodel

import android.content.Context
import android.util.Log
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import android.view.View
import android.view.ViewManager
import android.widget.Switch
import androidx.databinding.BindingAdapter
import androidx.databinding.Observable
import androidx.lifecycle.ViewModel
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import me.cocsos.lineword.BR
import me.cocsos.lineword.BaseApplication.Companion.sharedPreferences
import me.cocsos.lineword.BuildConfig
import me.cocsos.lineword.R

class SettingViewModel : BaseObservable() {

    private val TAG = javaClass.simpleName

    @get:Bindable
    var filter_a: Boolean = true
        set(value) {
            field = value
            notifyPropertyChanged(BR.filter_a)
        }

    @get:Bindable
    var filter_v: Boolean = true
        set(value) {
            field = value
            notifyPropertyChanged(BR.filter_v)
        }

    @get:Bindable
    var filter_n: Boolean = true
        set(value) {
            field = value
            notifyPropertyChanged(BR.filter_n)
        }

    @get:Bindable
    var filter_ad: Boolean = true
        set(value) {
            field = value
            notifyPropertyChanged(BR.filter_ad)
        }

    @get:Bindable
    var filter_prep: Boolean = true
        set(value) {
            field = value
            notifyPropertyChanged(BR.filter_ad)
        }

    @get:Bindable
    var filter_etc: Boolean = true
        set(value) {
            field = value
            notifyPropertyChanged(BR.filter_etc)
        }

    @get:Bindable
    var filter_elementary: Boolean = true
        set(value) {
            field = value
            notifyPropertyChanged(BR.filter_elementary)
        }

    @get:Bindable
    var filter_middle: Boolean = true
        set(value) {
            field = value
            notifyPropertyChanged(BR.filter_middle)
        }

    @get:Bindable
    var filter_normal: Boolean = true
        set(value) {
            field = value
            notifyPropertyChanged(BR.filter_normal)
        }
    @get:Bindable
    var filter_expert: Boolean = true
        set(value) {
            field = value
            notifyPropertyChanged(BR.filter_expert)
        }

    @get:Bindable
    var filter_memorize: Boolean = true
        set(value) {
            field = value
            notifyPropertyChanged(BR.filter_memorize)
        }

    @get:Bindable
    var filter_special_only: Boolean = true
        set(value) {
            field = value
            notifyPropertyChanged(BR.filter_special_only)
        }

    @get:Bindable
    var filter_wrong_only: Boolean = true
        set(value) {
            field = value
            notifyPropertyChanged(BR.filter_wrong_only)
        }
    @get:Bindable
    var no_ads: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.no_ads)
        }

//    @get:Bindable
//    var filter_a: Boolean = true
//        set(value) {
//            field = value
//            notifyPropertyChanged(BR.filter_a)
//        }


    fun loadOption(context: Context){

        filter_a = sharedPreferences.getBoolean(context.resources.getString(R.string.filter_a), true)
        filter_v = sharedPreferences.getBoolean(context.resources.getString(R.string.filter_v), true)
        filter_n = sharedPreferences.getBoolean(context.resources.getString(R.string.filter_n), true)
        filter_ad = sharedPreferences.getBoolean(context.resources.getString(R.string.filter_ad), true)
        filter_prep = sharedPreferences.getBoolean(context.resources.getString(R.string.filter_prep), true)
        filter_etc = sharedPreferences.getBoolean(context.resources.getString(R.string.filter_etc), true)

        filter_elementary = sharedPreferences.getBoolean(context.resources.getString(R.string.filter_elementary), true)
        filter_middle = sharedPreferences.getBoolean(context.resources.getString(R.string.filter_middle), true)
        filter_normal = sharedPreferences.getBoolean(context.resources.getString(R.string.filter_normal), true)
        filter_expert = sharedPreferences.getBoolean(context.resources.getString(R.string.filter_expert), true)

        filter_memorize = sharedPreferences.getBoolean(context.resources.getString(R.string.filter_memorize), true)
        filter_special_only = sharedPreferences.getBoolean(context.resources.getString(R.string.filter_special_only), false)
        filter_wrong_only = sharedPreferences.getBoolean(context.resources.getString(R.string.filter_wrong_only), false)
//        filter_a = sharedPreferences.getBoolean(context.resources.getString(R.string.filter_a), true)
//        filter_a = sharedPreferences.getBoolean(context.resources.getString(R.string.filter_a), true)

    }
    fun onOptionChanged(v: View,checked:Boolean) {
        when (v) {
            is Switch -> {
                val optionName = v.text.toString()
                sharedPreferences.edit().putBoolean(optionName, checked).apply()
                Log.d(TAG, "onOptionChanged: $checked")
            }
        }
    }


    companion object{


        @BindingAdapter("noads")
        @JvmStatic
        fun setAds(view: AdView, noads: Boolean?) {
            noads ?: return

            if (noads) {
                Log.d("setAds", "setAds: No ads!")

                view.visibility = View.GONE

            } else {
                if (BuildConfig.DEBUG) {
                    MobileAds.initialize(view.context, view.context.getString(R.string.adidtest))
                } else {
                    MobileAds.initialize(view.context, view.context.getString(R.string.adid))
                }

                view.loadAd(AdRequest.Builder().run {
                    if (BuildConfig.DEBUG) addTestDevice("230BCB4A1F1A3C8D5ACE7A9E76C969A0")
                    build()
                })
                view.adListener = object : AdListener() {
                    override fun onAdLoaded() {
                        super.onAdLoaded()
                        view.post {

                            view.translationY = view.height.toFloat()
                            view.visibility = View.VISIBLE
                            view.animate()
                                    .translationY(0f)
                                    .setDuration(500)
                                    .start()

                        }
                    }
                }

                // see 전면광고
//                    val mInterstitialAd: InterstitialAd = InterstitialAd(view.context).apply {
//                        adUnitId = getString(R.string.admob_unit_id_interstitial_test)
//                    }
//                    mInterstitialAd.loadAd(AdRequest.Builder()
//                            .addTestDevice("4FBA3E22D8C8E3F81A6820E388CD58C7")
//                            .build())

//                mInterstitialAd.adListener = object : AdListener() {
//                    override fun onAdLoaded() {}
//                    override fun onAdFailedToLoad(errorCode: Int) {}
//                    override fun onAdOpened() {}
//                    override fun onAdClicked() {}
//                    override fun onAdLeftApplication() {}
//                    override fun onAdClosed() {
//                        finish()
//                    }
//                }
            }
        }
    }
}
