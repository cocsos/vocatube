package me.cocsos.lineword.viewmodel

import android.content.res.AssetManager
import android.content.res.Resources
import androidx.databinding.BaseObservable
import android.util.Log
import com.google.android.gms.common.api.ResolvingResultCallbacks
import me.cocsos.lineword.BaseApplication
import me.cocsos.lineword.R
import me.cocsos.lineword.model.Word
import java.io.BufferedReader

/**
 * Created by lenovo on 2018-11-05.
 */

class DictationViewModel2 : BaseObservable() {
    private val TAG = javaClass.simpleName

    val multiDict = mutableListOf<Pair<String, String>>()   // format => -단어 /// 형태.뜻-
    val singleDict = mutableListOf<Pair<String, String>>()   // format => -단어 /// 형태.뜻-
    private val adverb = mutableListOf<Pair<String, String>>()   // format => -단어 /// 형태.뜻-

    private val levelList = mutableListOf<Pair<String, String>>()
    private val bookmark = HashMap<Char, Int>()

    //^[a-zA-Z]+ [a-zA-Z]+.*

    fun loadWord(rsc: Resources) {
        try {
            Log.d(TAG, ":init ")
            val rawId = R.raw.singleword53311
            val inputStream = rsc.openRawResource(rawId)
            val read = inputStream.bufferedReader().use(BufferedReader::readText)
            inputStream.close()

            val rawId2 = R.raw.adverb
            val inputStream2 = rsc.openRawResource(rawId2)
            val read2 = inputStream2.bufferedReader().use(BufferedReader::readText)
            inputStream2.close()

            val rawId3 = R.raw.multiword4818
            val inputStream3 = rsc.openRawResource(rawId3)
            val read3 = inputStream3.bufferedReader().use(BufferedReader::readText)
            inputStream3.close()

            Log.d(TAG, ": ${read2.split("\n").size} 개 단어")

            read.split("\n").forEach {
                val ss = it.split("\t")
                singleDict.add(Pair(ss[0].trim(), ss[1].trim()))
            }

            read2.split("\n").forEach {
                val ss = it.split("\t")
                adverb.add(Pair(ss[0].trim(), ss[1].trim()))
            }

            read3.split("\n").forEach {
                val ss = it.split("\t")
                multiDict.add(Pair(ss[0].trim(), ss[1].trim()))
            }

            val rawId4 = R.raw.ek3000
            val inputStream4 = rsc.openRawResource(rawId4)
            val read4 = inputStream4.bufferedReader().use(BufferedReader::readText)
            inputStream4.close()

            //level
            read4.split("\n").forEach {
                val ss = it.split("\t")
                levelList.add(Pair(ss[0].trim(), ss[2].trim()))
            }

            //색인을 빨리 하기 위해 a~z위치 를 책갈피
            addBookMark()

        } catch (e: Exception) {
            Log.e(TAG, "Error: ")
            e.printStackTrace()
        }
    }


    private fun addBookMark() {
        var c: Char = 'a'
        Log.d(TAG, "addBookMark: ")
        while (c <= 'z') {

            var ind = -1
            singleDict.forEachIndexed { index, pair ->
                if (ind == -1 && pair.first[0] == c) {
                    ind = index
                }
            }

            bookmark[c] = ind

            Log.d(TAG, "addBookMark: $c from $ind")
            ++c
        }
    }


    /**
     * 정제된 문자열의 단어를 넣으면 Word 객체를 출력하는 함수
     * @param str origin 단어 원문
     * @param _mean 단어의 뜻. 입력값이 없으면 검색해서 넣는다
     * @return Word 해당 단어가 사전에 있다면 Word 객체 생성 하여 리턴
     */
    fun makeWord(str: String, _mean: String? = null) = Word().apply {

        level = levelList.firstOrNull { it.first == str }?.second ?: ""
        origin = str
        display = str
        mean = _mean ?: singleDict.firstOrNull {
            it.first == str
        }?.second ?: multiDict.firstOrNull {
            it.first == str
        }?.second ?: "no data"

        if (mean.indexOf("a.") != -1 || mean.indexOf("adj.") != -1) type += 0b0001       //형용사
        if (mean.indexOf("v.") != -1) type += 0b0010      //동사
        if (mean.indexOf("n.") != -1 || mean.indexOf("pron.") != -1) type += 0b0100         //명사
        if (mean.indexOf("ad.") != -1) type += 0b1000         //부사

    }

    fun searchLine(_word: String): List<String> {
        val word = _word.replace("/[,.~!?]/", "")
        val startWith: Char = word[0].toLowerCase()

        val start = bookmark[startWith] ?: 0
        var end = bookmark[(startWith + 1)] ?: (singleDict.size - 1)

        Log.d(TAG, "searchLine: $word , $startWith indexing from $start to $end")

        if (end == -1) end = singleDict.size

        var r = singleDict.subList(start, end).filter { it.first == word }.map { it.second }

        if (adverb.any { it.first == word }) {
            r = adverb.filter { it.first == word }.map { it.second }
        }

        if (r.isEmpty()) {
            //TODO 후 처리할 것 조사
            //TODO 복수형(s혹은 es 처리)  소유격('s처리)
            //TODO 동사의 과거,pp형 처리
            //TODO 줄임말
        }

        return r
    }
//
//    fun translateMulti(_word: String): List<String> {
//        val word = _word.replace("/[,.~!?]/", "")
//        val startWith: Char = word[0].toLowerCase()
//
//        val start = bookmark[startWith] ?: 0
//        var end = bookmark[(startWith + 1)] ?: (singleDict.size - 1)
//
//        Log.d(TAG, "searchLine: $word , $startWith indexing from $start to $end")
//
//        if (end == -1) end = singleDict.size
//
//        var r = singleDict.subList(start, end).filter { it.first == word }.map { it.second }
//
//        if( adverb.any{it.first == word}) {
//           r =  adverb.filter { it.first == word }.map { it.second }
//        }
//
//        if(r.isEmpty()){
//            //TODO 후 처리할 것 조사
//            //TODO 복수형(s혹은 es 처리)  소유격('s처리)
//            //TODO 동사의 과거,pp형 처리
//            //TODO 줄임말
//        }
//
//        return r
//    }
}

