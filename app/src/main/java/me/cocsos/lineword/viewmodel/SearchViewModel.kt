package me.cocsos.lineword.viewmodel

import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.ViewParent
import android.view.animation.Animation
import android.view.animation.Transformation
import android.widget.LinearLayout
import android.widget.ScrollView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.BindingAdapter
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.viewpager.widget.ViewPager
import me.cocsos.lineword.BaseApplication
import me.cocsos.lineword.R
import me.cocsos.lineword.model.Word
import me.cocsos.lineword.ui.adapter.WordViewPagerAdapter
import me.cocsos.lineword.util.CircularViewPagerHandler


/**
 *
 * 데이터의 입력과 처리 데이터를 보관
 *
 * @property query FragmentInput의 et_input에 입력된 데이터(후처리 된 상태)
 * @property wordList 검색 결과로 나온 단어 리스트
 * @property progress 현재 진행상황을 나타내는 문자열
 * @property current 현재 선택된 위치를 가리키는 인덱스
 */
class SearchViewModel : ViewModel() {

    private val TAG = javaClass.simpleName

    var query = ""

    private val _wordList = MutableLiveData<List<Word>>()
    val wordList: LiveData<List<Word>> get() = _wordList

    private val _progress = MutableLiveData<String>()
    val progress: LiveData<String> get() = _progress

    private val _current = MutableLiveData<Int>().apply { value = -1 }
    val current: LiveData<Int> get() = _current

    lateinit var mRefreshAndNext: ((Int) -> (Unit))


    lateinit var dictationViewModel: DictationViewModel2

    val dictList = mutableListOf<Pair<Int, String>>()   //단어의 시작 위치와 그 단어 원형을 저장
    val result = mutableMapOf<String, String>()         //단어의 원형과 그 뜻을 저장


    fun setProgress(str: String) {
        _progress.postValue(str)
    }

    fun setCurrent(cur: Int) {
        _current.value = cur
    }

    fun setWordList(list: List<Word>) {
        _wordList.postValue(list)
    }

    /*검색 결과를 클리어하고 구문 분석을 기다림*/
    fun clearAndWait() {
        _wordList.postValue(null)
        _progress.postValue("구문을 분석합니다")
        _current.postValue(-1)
    }


    /**
     * 일단 따로 추가하자
     *1차 준비 -> \n제거, 여러개의 띄어쓰기 하나만 적용
     * 1차 여러단어를 찾은 후 pair<Int,String>( 해당 위치,한글 단어)
     * 2차 준비 -> 띄어쓰기 별로 나누기
     * 2차 각 단어로 교체 후 pair<Int,String>( 해당 위치,한글 단어)
     * "한글 단어".replace(";","\n")
     *
     * @return Pair<정리된 문자열, distList<위치, 문자열>> 리턴
     */
    fun addTransedWord(): Pair<String, List<Pair<Int, String>>> {

        result.clear()

        val pos = mutableMapOf<Int, String>()

        _progress.postValue("단어를 검색하는 중...") //숙어, 복합 단어 추출
        var text = query.replace("  ", " ")    //띄어쓰기 두개는 삭제
        text = text.replace("-\n", "")  //- 줄바꿈문자는 붙이기
        text = text.replace("\n", " ")   //줄바꿈 -> 띄어쓰기
        // SEE 줄바꿈을 띄어쓰기로 바꾸는 이유? 텍스트로 치면 알아서 단어끼리 내려감.

        var marker = text.toLowerCase()//모두 소문자로 변경
        val re = Regex("[^a-z0-9- ]")     //숫자는 봐준다.
        marker = re.replace(marker, " ")

//TODO 단수/복수, 현재과거pp, 소유의 's,여러개의 +s붙는것

        var multiIndex = 0

        dictationViewModel.multiDict.forEachIndexed { index, meanPair ->
            //띄어쓰기와 띄어쓰기 단어까지 같아야함!!
            while (marker.indexOf(meanPair.first, multiIndex) != -1) {

                val start: Int = if (index == 0) {
                    if (meanPair.first.length == marker.length) {
                        0
                    } else {
//                            Log.d(TAG, "addTransedWord: ${word.length}, ${it.second.length}")
                        marker.indexOf("${meanPair.first} ", multiIndex)
                    }
//                    marker.indexOf("${it.first} ")
                } else {
                    if (marker.indexOf(" ${meanPair.first} ", multiIndex) == -1) {
                        Log.e(TAG, "addTransedWord:cannot FOUND!! ${marker.indexOf(meanPair.first, multiIndex)}, ${meanPair.second}")

                        Log.d(TAG, "addTransedWord: from $marker")
                        return@forEachIndexed
                    }
                    marker.indexOf(" ${meanPair.first} ", multiIndex) + 1
                }

                result[meanPair.first] = meanPair.second
                pos[start] = meanPair.first

                val leng = meanPair.first.length

                multiIndex = start + leng
                Log.d(TAG, "addTransedWord:multi $start~${start + leng}${meanPair.second}->${meanPair.first}")
                val sb = StringBuilder()
                for (i in 0 until leng) sb.append(".")

                marker = marker.replaceRange(start until start + leng, sb.toString())

            }
        }

        Log.d(TAG, "addTransedWord:multidic after  $marker")

        //단일어 추출
        val words = marker.split(" ")
        var singleIndex = 0
        Log.d(TAG, "addTransedWord: 띄어쓰기 나온 결과 -> $words")
        words.forEach { word ->
            dictationViewModel.singleDict.forEachIndexed { _, meanPair ->
                if (TextUtils.equals(meanPair.first, word)) {      //같으면 추가하고 "."으로 교체
//                    val first = System.currentTimeMillis()
                    val start: Int = if (marker.indexOf(meanPair.first, singleIndex) == 0) {   //첫 단어. 앞 띄어쓰기 없음
                        if (meanPair.first.length == word.length) {
                            0
                        } else {
//                            Log.d(TAG, "addTransedWord: ${word.length}, ${it.second.length}")
                            marker.indexOf("${meanPair.first} ", singleIndex)
                        }
                    } else if (marker.indexOf(meanPair.first, singleIndex) + meanPair.first.length == marker.length) {   //마지막 단어. 뒤 띄어쓰기 없음
                        marker.indexOf(meanPair.first)
                    } else {
                        if (marker.indexOf(" ${meanPair.first} ", singleIndex) == -1) {  //교체할 단어 없음
//                            Log.e(TAG, "addTransedWord:cannot FOUND!! ${marker.indexOf(it.first, singleIndex)}, ${it.second}")
//                            Log.e(TAG, "addTransedWord:cannot FOUND!! 앞공백 ${marker.indexOf(" ${it.first}", singleIndex)}")
//                            Log.e(TAG, "addTransedWord:cannot FOUND!! 뒷공백${marker.indexOf("${it.first} ", singleIndex)}")
//                            Log.e(TAG, "addTransedWord:cannot FOUND!! ${marker.indexOf(word)}??")
//                            Log.e(TAG, "addTransedWord:cannot FOUND!!${it.first} == $word")
//
//                            Log.d(TAG, "addTransedWord: from $marker")
                            return@forEach
                        }

                        marker.indexOf(" ${meanPair.first} ", singleIndex) + 1
                    }
                    val sec = System.currentTimeMillis()

                    result[meanPair.first] = meanPair.second
                    pos[start] = meanPair.first


                    val leng = meanPair.first.length
                    singleIndex = start + leng


                    Log.d(TAG, "addTransedWord:single $start~${start + leng} $word->${meanPair.first}")
//                    val third = System.currentTimeMillis()
                    val sb = StringBuilder()

                    for (i in 0 until leng) sb.append(".")

                    marker = marker.replaceRange(start until start + leng, sb.toString())

//                    val end = System.currentTimeMillis()
//                    Log.d(TAG, "addTransedWord: 1-${sec - first} ,2-${third - sec}, 3-${end - third}}")
                }

            }
        }



        dictList.addAll(pos.toList().sortedBy { it.first })

        Log.d(TAG, "addTransedWord: $marker")
        return Pair(text, dictList)

    }


    companion object {


        //        app:expandCollapse="@{searchViewModel.current}"
        @BindingAdapter("expandCollapse")
        @JvmStatic
        fun expandCollapse(view: View, current: Int?) {

            current ?: return

            Log.d("expandCollapse", "expandCollapse: $current")

            fun expand(v: View) {
                v.measure(ConstraintLayout.LayoutParams.MATCH_CONSTRAINT, ConstraintLayout.LayoutParams.MATCH_CONSTRAINT)
                val targetHeight = v.measuredHeight

                // Older versions of android (pre API 21) cancel animations for views with a height of 0.
                v.layoutParams.height = 1
                v.visibility = View.VISIBLE
                val a = object : Animation() {
                    override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
                        v.layoutParams.height = if (interpolatedTime == 1f)
                            ConstraintLayout.LayoutParams.MATCH_CONSTRAINT
                        else
                            (targetHeight * interpolatedTime).toInt()
                        v.requestLayout()
                    }

                    override fun willChangeBounds(): Boolean {
                        return true
                    }
                }

                // 1dp/ms
                a.duration = (targetHeight / v.context.resources.displayMetrics.density).toInt().toLong() / 2
                v.startAnimation(a)
            }


            fun collapse(v: View) {
                val initialHeight = v.measuredHeight

                val a = object : Animation() {
                    override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
                        if (interpolatedTime == 1f) {
                            v.visibility = View.GONE
                        } else {
                            v.layoutParams.height = initialHeight - (initialHeight * interpolatedTime).toInt()
                            v.requestLayout()
                        }
                    }

                    override fun willChangeBounds(): Boolean {
                        return true
                    }
                }

                // 1dp/ms
                a.duration = (initialHeight / v.context.resources.displayMetrics.density).toInt().toLong() / 2
                v.startAnimation(a)
            }


            when (current) {
                -1 -> collapse(view)
                else -> {
                    if (view.visibility == View.VISIBLE) expand(view)
                }
            }
        }

        /**
         * app:wordList="@{searchViewModel.wordList}"
         * app:wordViewModel="@{wordViewModel}"
         */
        @BindingAdapter(value = ["wordList", "wordViewModel"], requireAll = true)
        @JvmStatic
        fun setWordListAdapter(vp: ViewPager, wordList: List<Word>?, wordViewModel: WordViewModel?) {
            wordList ?: return
            wordViewModel ?: return

            val adapter: WordViewPagerAdapter
            if (vp.adapter == null) {
                adapter = WordViewPagerAdapter(vp.context, wordViewModel)
                adapter.items = wordList
                vp.adapter = adapter
                vp.addOnPageChangeListener(CircularViewPagerHandler(vp))

            } else {
                adapter = vp.adapter as WordViewPagerAdapter
                adapter.items = wordList
                adapter.notifyDataSetChanged()
            }
//            adapter.refreshAndNext = searchViewModel.mRefreshAndNext
        }

        /**
         * app:currentWord="@{searchViewModel.current}"
         */
        @BindingAdapter("currentWord")
        @JvmStatic
        fun setCurrentWord(vp: ViewPager, current: Int?) {
            current ?: return

            val adapter = vp.adapter as? WordViewPagerAdapter ?: return

//            Log.d("setCurrentWord", "setCurrentWord: adapter.items.size = ${adapter.items.size} current= $current")
            if (adapter.items.isEmpty()) return
            if (adapter.items.size > current) {
                vp.currentItem = current
            }

        }

        /**
         * current에 맞춰 스크롤링
         *app:syncScroll="@{searchViewModel.current}"
         * app:childLayout="@{clWords}"
         */
        @BindingAdapter(value = ["syncScroll", "childLayout"])
        @JvmStatic
        fun syncScroll(sv: ScrollView, current: Int?, layout: LinearLayout?) {
            current ?: return

            val childCount = layout?.childCount ?: return

            for (i in 0 until childCount) {
                val line = layout.getChildAt(i) as? LinearLayout?

                val wordCount = line?.childCount ?: continue
                for (item in 0 until wordCount) {
                    val parent = line.getChildAt(item)
                    if (parent?.findViewById<TextView>(current) != null) {
                        Log.d("syncScroll", "syncScroll: to ${line.top}")
                        sv.smoothScrollTo(0, (line.top - layout.height / 2))
                        return
                    }
                }
            }
        }

        /**
         * 검색된 단어를 표시
         * app:myList="@{wordViewModel.myWordList}"
         * app:markWord="@{word}"
         * app:current="@{searchViewModel.current}"
         *
         * 선택상태면 무조건 선택보여주기.
         * null or false라면 배경 없음
         * 나머지는 상태에 따라서 분기
         */
        @BindingAdapter(value = ["myList", "markWord", "current"], requireAll = true)
        @JvmStatic
        fun markSavedWord(tv: TextView, myWordList: List<Word>?, word: Word?, current: Int?) {
            current ?: return
//            myWordList ?: return
//            Log.d("markSavedWord", "markSavedWord: current = $current id = ${tv.id} ")
            when {
                word == null -> tv.setBackgroundResource(0)
                tv.id == current -> tv.setBackgroundResource(R.drawable.round_corner_border_selected)
                word.origin == "-_-" -> tv.setBackgroundResource(0)
                else -> {   //현재 아이템이
                    val item = myWordList?.firstOrNull { it.origin == word.origin }
                    when {
                        item == null -> tv.setBackgroundResource(R.drawable.underline_red)
                        item.special -> {
                            if (BaseApplication.sharedPreferences.getBoolean("show_special", true))
                                tv.setBackgroundResource(R.drawable.purple)
                            else tv.setBackgroundResource(0)

                        }
                        item.memorized -> {
                            if (BaseApplication.sharedPreferences.getBoolean("show_memorize", true))
                                tv.setBackgroundResource(R.drawable.green)
                            else tv.setBackgroundResource(0)
                        }
                        item.wrong -> {
                            if (BaseApplication.sharedPreferences.getBoolean("show_wrong", true))
                                tv.setBackgroundResource(R.drawable.red)
                            else tv.setBackgroundResource(0)
                        }
                        else -> tv.setBackgroundResource(R.drawable.yellow)//그냥 추가만 된 상태!
                    }
                }
            }

        }


    }
}