package me.cocsos.lineword.viewmodel

import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import androidx.databinding.BindingAdapter
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import me.cocsos.lineword.BuildConfig
import me.cocsos.lineword.model.Word
import me.cocsos.lineword.ui.adapter.WordAdapter

class MainViewModel : ViewModel() {
    private val TAG = javaClass.simpleName

    private val _word = MutableLiveData<Word>()
    val word: LiveData<Word> get() = _word


    fun setWord(w: Word?) {
        _word.value = w
    }

    companion object {
        /**
         * updateWord 기존 단어를 삭제하고 새 단어로 교체하여 저장 *삭제하는 이유? property 설정하기 귀찮아서.
         * 기존 단어가 없으면 추가함
         * @param newWord 교체될 새 단어. 뜻이 바뀌는 것은 아니고 단어 설정이 바뀌게 된다.
         *
         */
        @BindingAdapter(value = ["wordList", "settingViewModel"])
        @JvmStatic
        fun setWordList(view: View, item: List<Word>?, settingViewModel: SettingViewModel?) {
            item ?: return
            settingViewModel ?: return
            val adapter: WordAdapter
            if ((view as RecyclerView).adapter == null) {
                adapter = WordAdapter(view.context)

                view.adapter = adapter
                view.layoutManager = LinearLayoutManager(view.context, RecyclerView.VERTICAL, false)
            } else {
                adapter = view.adapter as WordAdapter
            }


            adapter.items.clear()

            Log.d("setWordList", "setWordList: before = ${item}")
            Log.d("setWordList", "setWordList: before size = ${item.size}")
            fun listFilter(list: List<Word>, setting: SettingViewModel): List<Word> {
                // 형식 필터링 ADJECT(0b00001), VERB(0b00010), NOUN(0b00100), ADVERB(0b01000)
                val visibleList = mutableListOf<Word>()  //보여줄 리스트
                var sortFilter = 0

                if (setting.filter_a) sortFilter += 0b00001
                if (setting.filter_v) sortFilter += 0b00010
                if (setting.filter_n) sortFilter += 0b00100
                if (setting.filter_ad) sortFilter += 0b01000
                if (setting.filter_prep) sortFilter += 0b10000

                return list.filter {
                    //타입 필터링
                    Log.d("setWordList", "listFilter: 타입 필터링 ${it}")
                    if (it.type == 0) setting.filter_etc else it.type and (sortFilter) != 0b00000
                }.filter {
                    Log.d("setWordList", "listFilter: 난이도 필터링 ${it}")
                    // 난이도 필터링
                    when (it.level) {
                        "초등" -> setting.filter_elementary
                        "중고" -> setting.filter_middle
                        "전문" -> setting.filter_expert
                        "" -> setting.filter_normal
                        else -> false
                    }
                }
//                        .filter {
//                    Log.d("setWordList", "listFilter: 암기/특별/틀림 필터링 ${it}")
//                    //암기완료 보기
//                    (if (it.memorized) setting.filter_memorize else true)
//                            ||
//                            //특별 단어만 보기
//                            (if (it.special) setting.filter_special_only else true)
//                            ||
//                            //틀린 단어만 보기
//                            (if (it.wrong) setting.filter_wrong_only else true)
//
//
//                }
            }

            val filtered = listFilter(item, settingViewModel)
            Log.d("filtered", "setWordList: filtered = ${filtered}")
            Log.d("filtered", "setWordList: filtered.size = ${filtered.size}")
            adapter.items.addAll(filtered)


            adapter.notifyDataSetChanged()
        }

        /**
         *  내 단어가 비었을경우 표시.
         *
         */
        @BindingAdapter("wordListIsEmpty")
        @JvmStatic
        fun wordListIsEmpty(view: View, item: List<Word>?) {
            if (item == null || item.isEmpty()) {
                view.visibility = View.VISIBLE
            } else {
                view.visibility = View.GONE
            }
        }

        /**
         *  메인 화면에서 보여줄 검색 결과
         */
        @BindingAdapter("showWordResult")
        @JvmStatic
        fun showWordResult(view: View, word:Word?) {
            if (word != null ) {
                view.visibility = View.VISIBLE
            } else {
                view.visibility = View.GONE
            }
        }

        /**
         *  메인 화면에서 단어 검색을 위한 자동완성 미리보기 표시
         */
        @BindingAdapter("completeWord")
        @JvmStatic
        fun setAutoCompleteTextView(tv: AutoCompleteTextView, dictationViewModel: DictationViewModel2?) {
            dictationViewModel ?: return

            dictationViewModel.let {


                tv.setAdapter(ArrayAdapter(tv.context, android.R.layout.simple_dropdown_item_1line, it.singleDict.map { sd -> sd.first }+it.multiDict.map { md -> md.first } ))

                tv.threshold = 1
                Log.d("setAutoCompleteTextView", "setAutoCompleteTextView: word set")
            }

        }



    }

}
