package me.cocsos.lineword.viewmodel

import android.content.Context
import android.speech.tts.TextToSpeech
import com.google.android.material.snackbar.Snackbar
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import me.cocsos.lineword.R
import me.cocsos.lineword.model.*
import me.cocsos.lineword.util.RepoDatabase
import java.util.*

/**
 *
 * 단어를 추가하거나, 재생하거나
 * @property myWordList 그냥 추가되었든 특별 단어든 저장이 된 단어는 모두 저장
 */
class WordViewModel() : ViewModel() {
    private val _myWordList = MutableLiveData<List<Word>>()
    val myWordList: LiveData<List<Word>> get() = _myWordList

    private val TAG = javaClass.simpleName

    private lateinit var tts: TextToSpeech


    //저장된 단어를 가져와서 내 단어리스트 지정
    fun loadWords(context: Context) = CoroutineScope(Dispatchers.Default).launch {
        _myWordList.postValue(withContext(CoroutineScope(Dispatchers.Default).coroutineContext) {
            RepoDatabase.getInstance(context)
                    .wordDao.wordsNow
        })
    }


    //단어를 기존 리스트에 추가한다.
    fun add(context: Context, item: Any) {
        var old = _myWordList.value ?: listOf()
        when (item) {
            is List<*> -> {
                val list = item.filterIsInstance<Word>()
                val filtered = list.filter{
                    it.origin !in old.map{o->o.origin}  //원래 리스트와 겹치지 않은 데이터를 추가
                }
                filtered.updateAllAsync(context)
                old = old + list
            }
            is Word -> {
                item.update(context)
                old = old + item
            }
        }
        _myWordList.value = old.distinct()
    }


    //단어를 기존 리스트에 추가한다.
    fun remove(context: Context, item: Any) {
        var old = _myWordList.value ?: listOf()
        when (item) {
            is List<*> -> {
                //현재 리스트 중 있는 아이템만 필터링
                val list = item.filterIsInstance<Word>().filter { it.origin in old.map { o -> o.origin } }
                for (word in list) {
                    word.delete(context)
                }
                old = old - list
            }
            is Word -> {
                if (item.origin !in old.map { o -> o.origin }) return
                item.update(context)
                old = old - item
            }
        }
        _myWordList.value = old.distinct()
    }


    /**
     * 선택된 단어를 하나 Room에 저장한다
     * MutableList<Word> 형 리스트를 이용해조작.
     * 받아올때 fromJson(문자열,MutableList<Word>::class.java)가 아닌
     * object : TypeToken<List<Word>>() {}.type임에 유의
     *@param view 흔드는 애니메이션을 위해 사용
     */
    fun addWord(view: View, word: Word) {

        val anim = AnimationUtils.loadAnimation(view.context, R.anim.anim_shake)
        view.startAnimation(anim)

        CoroutineScope(Dispatchers.Main).launch {
            val original = withContext(Dispatchers.Default) {
                word.isExistNow(view.context)
            }
            if (original == null) {
                val result = withContext(Dispatchers.Default) { word.updateNow(view.context) }
                if (result != null) {
                    Snackbar.make(view, "'${word.origin}' 추가했습니다", Snackbar.LENGTH_SHORT).show()
                    add(view.context, word)
                }
            } else {
                Snackbar.make(view, "'${word.origin}' 이미 추가된 단어입니다", Snackbar.LENGTH_SHORT).show()
            }
        }
    }


    /** 현재 선택된 단어가 있을 때 말하는 중이 아니면 말하기 시작. 실패가능
     *@param view TextToSpeech 객체 생성에 필요한 context를 위해 받음
     */
    fun playWord(view: View, word: Word) {
        if (!::tts.isInitialized) {
            val listener = TextToSpeech.OnInitListener { status ->
                if (status == TextToSpeech.SUCCESS) {
                    Log.d("OnInitListener", "Text to speech engine started successfully.")
                    tts.language = Locale.US
                } else {
                    Log.d("OnInitListener", "Error starting the text to speech engine.")
                }
            }
            tts = TextToSpeech(view.context, listener)
        }

        if (!tts.isSpeaking && !TextUtils.isEmpty(word.origin)) {
            Log.d(TAG, "playWord _ ${word.origin}")
            tts.speak(word.origin, TextToSpeech.QUEUE_ADD, null, "DEFAULT")
        }
    }


    fun onSpecialButtonClicked(v: View, dst: Word) {

        val anim = AnimationUtils.loadAnimation(v.context, R.anim.anim_shake)

        dst.special = !dst.special

        if (!dst.special) Snackbar.make(v, "'${dst.origin}' 특별 단어에서 해제합니다", Snackbar.LENGTH_SHORT).show()
        else {
            add(v.context, dst) //일치하는게 없다면 추가
            Snackbar.make(v, "'${dst.origin}' 특별 단어로 설정합니다", Snackbar.LENGTH_SHORT).show()
        }


        v.startAnimation(anim)

    }

    fun onWrongButtonClicked(v: View, dst: Word) {
        val anim = AnimationUtils.loadAnimation(v.context, R.anim.anim_shake)

        dst.wrong = !dst.wrong
        if (!dst.wrong) Snackbar.make(v, "'${dst.origin}' 시험에서 틀린 단어를 해제합니다", Snackbar.LENGTH_SHORT).show()
        else {
            add(v.context, dst) //일치하는게 없다면 추가
            Snackbar.make(v, "'${dst.origin}' 틀렸던 단어로 설정합니다", Snackbar.LENGTH_SHORT).show()
        }
        v.startAnimation(anim)
    }


    fun onSetMemorizedTrue(v: View, dst: Word) {
        if (dst.memorized) {
            Snackbar.make(v, "'${dst.origin}' 이미 외운 단어입니다.", Snackbar.LENGTH_SHORT)
                    .setAction(v.resources.getString(R.string.cancel)) {
                        dst.delete(v.context)
                    }.show()
            return
        }
        dst.memorized = true
        dst.update(v.context)

        Snackbar.make(v, "'${dst.origin}' 외운 단어에 추가했습니다", Snackbar.LENGTH_SHORT)
                .setAction(v.resources.getString(R.string.cancel)) {
                    dst.delete(v.context)
                }
                .show()
    }


    fun onMemorizedButtonClicked(v: View, dst: Word) {


        val anim = AnimationUtils.loadAnimation(v.context, R.anim.anim_shake)

        dst.let {
            if (it.memorized) Snackbar.make(v, "'${it.origin}' 외운 단어에서 해제합니다", Snackbar.LENGTH_SHORT).show()
            else Snackbar.make(v, "'${it.origin}' 외운 단어에 추가했습니다", Snackbar.LENGTH_SHORT).show()
        }
        v.startAnimation(anim)
    }

}