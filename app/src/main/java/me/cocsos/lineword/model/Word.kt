package me.cocsos.lineword.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import android.content.Context
import android.util.Log
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import me.cocsos.lineword.util.RepoDatabase

/**
 *
 * 단어 객체
 * @property origin 원형. ex) expect
 * @property display 보이는 단어. ex) expected
 * @property mean 뜻 ex) 예상하다
 * @property level 수준 ex) 초등 = 초등필수 단어
 * @property example 예문. 단어를 추가할때 예문도 추가한다
 * @property type 단어의 형태. ex)00000(미지정)~11111 이진수범위로, 00001이면 형용사,10000이면 전치사, ... , 00011이면 형용사&동사 가능한 단어.
 * @property memorized 암기 완료단어.
 * @property special 특별 관리단어
 * @property wrong 시험에서 틀린적 있는 단어
 */

//TODO 단어별 등장 횟수(사용자 데이터)
@Entity
class Word {
    @PrimaryKey
    var origin: String = ""
    var display: String = ""
    var mean: String = ""

    var level: String = ""  //초등/중고
    var example: String = ""

    var type: Int = 0b0


    var memorized: Boolean = false
    var special: Boolean = false
    var wrong: Boolean = false


    override fun toString(): String {
        return "Word(origin='$origin', display='$display', mean='$mean', level='$level', example='$example', type=${Integer.toBinaryString(type)}, memorized=$memorized, special=$special, wrong=$wrong)"
    }

    override fun equals(other: Any?): Boolean {
        return this.origin == (other as? Word)?.origin
    }

    override fun hashCode(): Int {
        return origin.hashCode()
    }
    companion object
}

fun Word.update(context: Context) {
    val d = Observable.just(
            RepoDatabase.getInstance(context)
                    .wordDao)
            .subscribeOn(Schedulers.io())
            .map { it.insert(this) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                Log.d("Word", "update: inserted row at $it")

            }, { Log.d("Word", "update: error $it.message") })
}
fun Word.updateAsync(context: Context)  = CoroutineScope(Dispatchers.Default).launch {
    RepoDatabase.getInstance(context)
            .wordDao.insert(this@updateAsync)
}
fun List<Word>.updateAllAsync(context: Context)  = CoroutineScope(Dispatchers.Default).launch {
    RepoDatabase.getInstance(context)
            .wordDao.insertAll(this@updateAllAsync)
}

fun Word.delete(context: Context) {
    val d = Observable.just(
            RepoDatabase.getInstance(context)
                    .wordDao)
            .subscribeOn(Schedulers.io())
            .map { it.delete(this.origin) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                Log.d("Word", "delete: deleted rows... $it")
            }, { Log.d("Word", "delete: error $it.message") })
}

fun Word.isExist(context: Context): Single<Word> =
        RepoDatabase.getInstance(context)
                .wordDao.getWord(this.origin)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())


/**
 * 단어의 원형이 저장되었는지 판단
 */
fun String.isOriginSaved(context: Context): Single<Word> =
        RepoDatabase.getInstance(context)
                .wordDao.getWord(this)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())


fun Word.isExistNow(context: Context): Word? =
        RepoDatabase.getInstance(context)
                .wordDao.getWordNow(this.origin)


fun Word.updateNow(context: Context) =
        RepoDatabase.getInstance(context)
                .wordDao.insert(this)

