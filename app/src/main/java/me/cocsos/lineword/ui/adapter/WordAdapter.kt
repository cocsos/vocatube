package me.cocsos.lineword.ui.adapter

import android.content.Context
import androidx.databinding.DataBindingUtil
import androidx.constraintlayout.widget.ConstraintLayout
import com.google.android.material.snackbar.Snackbar
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.Transformation
import android.widget.Toast
import me.cocsos.lineword.R
import me.cocsos.lineword.model.Word
import me.cocsos.lineword.databinding.ItemWordBinding
import me.cocsos.lineword.model.delete
import me.cocsos.lineword.model.update

class WordAdapter(val context: Context) : androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {
    val TAG: String = this.javaClass.simpleName

    val items = mutableListOf<Word>()
    private val opened = mutableMapOf<Int, Boolean>()

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {
//        return if (p1 == 1) {
        val bind = DataBindingUtil.inflate<ItemWordBinding>(LayoutInflater.from(p0.context), R.layout.item_word, p0, false)
        return ItemWordViewHolder(bind, bind.root)
//        } else {
//            val bind = DataBindingUtil.inflate<ItemWordBlankBinding>(LayoutInflater.from(p0.context), R.layout.item_word_blank, p0, false)
//            ItemWordBlankViewHolder(bind, bind.root)
//        }
    }


    fun remove(position: Int) {
        Log.d(TAG, "remove: $position")

        if (position == -1) {
            Toast.makeText(context, "다시 시도해주세요", Toast.LENGTH_SHORT).show()
            return
        }
        val item = items[position]
        item.delete(context)
        items.removeAt(position)

        notifyItemRemoved(position)
    }

    override fun getItemCount() = items.size

//    override fun getItemViewType(position: Int) =
//            if (items.size == 1) 2
//            else 1


    /**
     * onBindViewHolder
     * 중요단어
     */
    override fun onBindViewHolder(p0: androidx.recyclerview.widget.RecyclerView.ViewHolder, p1: Int) {

        when (p0) {
            is ItemWordViewHolder -> {
                val bind = p0.binding
                bind.word = items[p1]

                if (opened[p1] == true) expand(bind.clContent)
                else collapse(bind.clContent)

                bind.clContainer.setOnClickListener {
                    if (opened[p1] == true) {
                        opened[p1] = false
                        collapse(bind.clContent)
                    } else if (opened[p1] == false || opened[p1] == null) {
                        opened[p1] = true
                        expand(bind.clContent)
                    }
                }

                //틀림 추가/취소
                bind.ivWrong.setOnClickListener { v ->

                    if (items[p1].wrong) Snackbar.make(v, "'${items[p1].origin}' 틀렸던 단어에서 해제합니다", Snackbar.LENGTH_SHORT).show()
                    else Snackbar.make(v, "'${items[p1].origin}' 틀렸던 단어에 추가했습니다", Snackbar.LENGTH_SHORT).show()
                    items[p1].wrong = !items[p1].wrong
                    bind.word = items[p1]

                    items[p1].update(v.context)

                    val anim = AnimationUtils.loadAnimation(v.context, R.anim.anim_shake)
                    v.startAnimation(anim)

                }
                //암기 추가/취소
                bind.clContainer.setOnLongClickListener { v ->

                    if (items[p1].memorized) Snackbar.make(v, "'${items[p1].origin}' 외운 단어에서 해제합니다", Snackbar.LENGTH_SHORT).show()
                    else Snackbar.make(v, "'${items[p1].origin}' 외운 단어에 추가했습니다", Snackbar.LENGTH_SHORT).show()
                    items[p1].memorized = !items[p1].memorized
                    bind.word = items[p1]

                    items[p1].update(v.context)

                    val anim = AnimationUtils.loadAnimation(bind.tvTitle.context, R.anim.anim_shake)
                    bind.tvTitle.startAnimation(anim)

                    return@setOnLongClickListener true
                }
                //주의 추가/취소
                bind.ivSpecial.setOnClickListener {

                    if (items[p1].special) Snackbar.make(it, "'${items[p1].origin}' 특별 단어에서 해제합니다", Snackbar.LENGTH_SHORT).show()
                    else Snackbar.make(it, "'${items[p1].origin}' 특별 단어에 추가했습니다", Snackbar.LENGTH_SHORT).show()


                    items[p1].special = !items[p1].special
                    bind.word = items[p1]

                    val anim = AnimationUtils.loadAnimation(it.context, R.anim.anim_shake)
                    it.startAnimation(anim)


                    items[p1].update(it.context)
                }
            }
//            is ItemWordBlankViewHolder -> {
//            }
        }
    }


//    var position = 0
//    private fun showWordPagerDialog(p0: Int) {
//        this.position = p0
//
//        val bind = DataBindingUtil.inflate<DialogWordPagerBinding>(LayoutInflater.from(activity), R.layout.dialog_word_pager, null, false)
//        val pagerDialog = AlertDialog.Builder(activity)
//                .setView(bind.root)
//                .create()
//
//        pagerDialog.setCancelable(true)
//
//
//        bind.vpWord.adapter = WordPager()
//        bind.vpWord.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
//
//            override fun onPageScrollStateChanged(p0: Int) {}
//
//            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {}
//
//            override fun onPageSelected(p0: Int) {
//                val next = if (position >= items.size) items[0] else items[position]
//                position++
//                (bind.vpWord.adapter as WordPager).wordViewModel.word = next
//
//            }
//        })
//        pagerDialog.show()
//    }

    private fun expand(v: View) {
        v.measure(ConstraintLayout.LayoutParams.MATCH_CONSTRAINT, ConstraintLayout.LayoutParams.WRAP_CONTENT)
        val targetHeight = v.measuredHeight

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.layoutParams.height = 1
        v.visibility = View.VISIBLE
        val a = object : Animation() {
            override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
                v.layoutParams.height = if (interpolatedTime == 1f)
                    ConstraintLayout.LayoutParams.WRAP_CONTENT
                else
                    (targetHeight * interpolatedTime).toInt()
                v.requestLayout()
            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }

        // 1dp/ms
        a.duration = (targetHeight / v.context.resources.displayMetrics.density).toInt().toLong() / 2
        v.startAnimation(a)
    }


    private fun collapse(v: View) {
        val initialHeight = v.measuredHeight

        val a = object : Animation() {
            override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
                if (interpolatedTime == 1f) {
                    v.visibility = View.GONE
                } else {
                    v.layoutParams.height = initialHeight - (initialHeight * interpolatedTime).toInt()
                    v.requestLayout()
                }
            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }

        // 1dp/ms
        a.duration = (initialHeight / v.context.resources.displayMetrics.density).toInt().toLong() / 2
        v.startAnimation(a)
    }


    inner class ItemWordViewHolder(var binding: ItemWordBinding, itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView)
//    inner class ItemWordBlankViewHolder(var binding: ItemWordBlankBinding, itemView: View) : RecyclerView.ViewHolder(itemView)
}
