package me.cocsos.lineword.ui.fragment

import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.viewpager.widget.ViewPager
import kotlinx.coroutines.*
import me.cocsos.lineword.R
import me.cocsos.lineword.databinding.FragmentResultBinding
import me.cocsos.lineword.databinding.TextbrickBinding
import me.cocsos.lineword.model.Word
import me.cocsos.lineword.ui.activity.SearchActivity
import me.cocsos.lineword.viewmodel.DictationViewModel2
import me.cocsos.lineword.viewmodel.SearchViewModel
import me.cocsos.lineword.viewmodel.WordViewModel
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.sharedViewModel


class ResultFragment : androidx.fragment.app.Fragment(), View.OnClickListener {

    private val TAG = this.javaClass.simpleName
    lateinit var binding: FragmentResultBinding

    private val dictationViewModel2 by inject<DictationViewModel2>()
    private val searchViewModel by sharedViewModel<SearchViewModel>()
    lateinit var wordViewModel: WordViewModel


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        super.onCreate(savedInstanceState)
        Log.e(TAG, "--ResultFragment--")
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_result, container, false)
        binding.wvm = wordViewModel
        binding.searchViewModel = searchViewModel.apply {
            this.dictationViewModel = dictationViewModel2
        }
        binding.lifecycleOwner = this

        setButtonsAndSettings()
        return binding.root
    }

    private fun setButtonsAndSettings() {

//        binding.tvBack.setOnClickListener(this)
        binding.ivLeft.setOnClickListener(this)
        binding.ivRight.setOnClickListener(this)
        binding.ivAddAll.setOnClickListener(this)
        binding.clContainer.setOnClickListener(this)
        binding.ivOption.setOnClickListener(this)
        binding.svScroll.overScrollMode = View.OVER_SCROLL_ALWAYS


        binding.vpWord.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(p0: Int) {}
            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {}

            override fun onPageSelected(p0: Int) {
                searchViewModel.setCurrent(p0)
            }
        })

        searchViewModel.mRefreshAndNext = { it ->
            CoroutineScope(Dispatchers.Main).launch {
                delay(20)
                next()
            }
        }

    }


    override fun onClick(v: View?) {
        when (v?.id) {
//            R.id.tv_back -> onBackbuttonClick()
            R.id.iv_left -> behind()
            R.id.iv_right -> next()
            R.id.iv_add_all -> addAll()
            R.id.iv_option -> (activity as SearchActivity).showSettingDialog()
            R.id.cl_container -> searchViewModel.setCurrent(-1)
        }
    }

    fun startAnalyse() = CoroutineScope(Dispatchers.Default).launch {
        val anal = async {

            //  이전 검색 기록 지우기
            searchViewModel.clearAndWait()


            // 인풋을 번역한 결과
            val result = searchViewModel.addTransedWord()

//            if (result.second.isEmpty()) {
//                (activity as? SearchActivity)?.back()
//            }

            withContext(Dispatchers.Main) {

                drawText(result.first, result.second)
            }
            binding.tvProgress.visibility = View.GONE
        }

        anal.await()    //비동기 적으로 분석
    }


    override fun onResume() {
        super.onResume()

        view?.isFocusableInTouchMode = true;
        view?.requestFocus();
        view?.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
                if (keyCode == KeyEvent.KEYCODE_BACK && event?.action == KeyEvent.ACTION_UP) {
                    when {
                        binding.vpWord.visibility == View.GONE -> onBackbuttonClick()
                        binding.vpWord.visibility == View.VISIBLE -> {
                            searchViewModel.setCurrent(-1)
                            return false
                        }
                    }
                    return true
                }
                return false
            }
        })
    }


    /**
     * 검출된 단어들의 리스트인 dictList와 맞는 단위로 단어를 끊어서 cl_words에 추가한다
     * 추가된 단어는 brickList : mutableMapOf<Int, TextbrickBinding>()에 저장되니
     * 뷰페이저와 단어의 자체 뷰가 연동되도록 onClick에서 잘 처리하자.
     *
     * @param dictList 를 순회하는  index로 position를 사용
     * @param text 원본 글자를 indexing하는 offset 범위는 [0 ~ text.length)
     *
     * listOf<Word>에 생성된 Word객체를 담아  searchViewModel.wordList에 저장한다
     * 이후  executePendingBinding()을 호출하여 vp_word를 갱신시킨다.
     *
     */
    private fun drawText(text: String, dictList: List<Pair<Int, String>>) {
        var layer = LinearLayout(context)
        layer.orientation = LinearLayout.HORIZONTAL
        var params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        layer.layoutParams = params


        var layerWidth = 0

        // 생성된 단어 객체를 담을 임시 변수
        val tempList = mutableListOf<Word>()

        var position = 0
        var offset = 0

        Log.d(TAG, "dicList =${dictList}")



        while (offset < text.length && position <= dictList.size - 1) {


            //ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡ실제 단어와 매칭
            var w: String
            val find: Boolean

            val posAndOrigin = dictList[position] //단어가 나올 위치
            when {
                offset < posAndOrigin.first -> {    //현재 위치가 다음 단어를 바꾸지 못하면
                    Log.d(TAG, "addTransedWord: not found! offset = $offset , until ${posAndOrigin.first}")
                    w = text.substring(offset until (offset + 1))   //한 글자씩? 교체
                    offset += 1
                    find = false
                }
                offset == posAndOrigin.first -> {
                    Log.d(TAG, "addTransedWord: found! offset = $offset , until ${posAndOrigin.first + posAndOrigin.second.length}")
                    w = text.substring(posAndOrigin.first until (posAndOrigin.first + posAndOrigin.second.length))
                    position++
                    offset = posAndOrigin.first + posAndOrigin.second.length
                    find = true
                }
                else -> {
                    Log.d(TAG, "addTransedWord: offset>it.first")
                    return
                }
            }

            Log.d(TAG, "addTransedWord: ${"%.1f".format(100F * offset.toFloat() / text.length.toFloat())}% 변환중 $offset/${text.length}")
            searchViewModel.setProgress("%.1f%%".format(100F * offset.toFloat() / text.length.toFloat()))

//ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡTextView 생성 파트

            val bind = DataBindingUtil.inflate<TextbrickBinding>(LayoutInflater.from(context), R.layout.textbrick, null, false)


            Log.d(TAG, "addTransedWord: creating bricks... $w at ${bind.tvOrigin.id}")

            val word: Word

            if (find) { //의미가 있는 단어만

                bind.tvOrigin.id = position - 1


                word = dictationViewModel2.makeWord(posAndOrigin.second,
                        searchViewModel.result[posAndOrigin.second] ?: "no data")

                word.display = w

                Log.d(TAG, "drawText: word.type = ${word.type}")

                tempList.add(word)

                bind.word = word

                when (word.level) {
                    "초등" -> {
//                        bind.tvLevel.setBackgroundColor(ContextCompat.getColor(context, R.color.yellow))
//                        tv.setTextColor(ContextCompat.getColor(context, R.color.yellow))
                    }
                    "중고" -> {
//                        bind.tvLevel.setBackgroundColor(Color.parseColor("#7CFF95"))
//                        tv.setTextColor(ContextCompat.getColor(context, R.color.yellow))
                    }
                    "전문" -> {
//                        bind.tvLevel.setBackgroundColor(Color.parseColor("#FF4B00"))
//                        tv.setTextColor(ContextCompat.getColor(context, R.color.yellow))
                    }
                    else -> {
//                        tv.setTextColor(ContextCompat.getColor(context, android.R.color.black))
                    }
                }

                //뜻이 있을때는
                if (word.mean != "...") {
                    Log.d(TAG, "drawText: word.mean =${word.mean}")
                    bind.tvOrigin.setOnClickListener {
                        Log.d(TAG, "onClick setCurrent: ${it.id}")
                        //보여줄 데이터 변경
                        searchViewModel.setCurrent(it.id)

                        //해당 위치로 스크롤
                        binding.svScroll.smoothScrollTo(0, it.y.toInt() - (binding.svScroll.height / 2))    //현재 위치보다 반을 올려야 중앙에 위치
                    }

                } else {
                }

            } else {
                word = Word()
                word.origin = "-_-"     //비어있는 데이터를 표시
                bind.word = word
                bind.tvOrigin.setTextColor(ContextCompat.getColor(context!!, R.color.lightGray))
            }

            bind.tvOrigin.text = word.display           //SEE 너비 측정을 위해 databinding으로 넣지 않고 직접 지정
            bind.searchViewModel = searchViewModel
            bind.wordViewModel = wordViewModel
            bind.lifecycleOwner = this
//            bind.tvOrigin.setTextSize(TypedValue.COMPLEX_UNIT_PX, tsize)  //SEE 못찾았으면 글씨 크기 지울지?

            //ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡLayout에 추가하는 부분

            bind.root.measure(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)

            val width = bind.root.measuredWidth


            if (layerWidth + width < resources.displayMetrics.widthPixels - (resources.getDimensionPixelSize(R.dimen._20sdp) * 2)) {   //다 들어가면 통과
                layerWidth += width
                Log.d(TAG, "addTransedWord: adding ${w}, $layerWidth")
                layer.addView(bind.root)

            } else {  //안들어가면
                Log.d(TAG, "addTransedWord: new/ adding ${bind.tvOrigin.text}")
                binding.clWords.addView(layer) // 일단 넣고
                //새 줄 만든뒤 추가
                layer = LinearLayout(context)
                layer.orientation = LinearLayout.HORIZONTAL
                params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                layer.layoutParams = params

                layer.addView(bind.root)
                layerWidth = bind.root.measuredWidth
            }

            if (offset >= text.length || position == dictList.size) {//마지막이면
                binding.clWords.addView(layer)//열을 추가하며 끝.
            }

            bind.tvOrigin.tag = binding.clWords.childCount  //scroll을 위해 각 층을 저장
        }

        searchViewModel.setWordList(tempList)
        binding.searchViewModel = searchViewModel   //viewpager
        binding.executePendingBindings()

    }


    private fun behind() {
        var pos = binding.vpWord.currentItem - 1
        if (pos == -1) pos = searchViewModel.wordList.value?.size ?: return
        binding.vpWord.currentItem = pos
    }

    private fun next() {
        var pos = binding.vpWord.currentItem + 1
        if (pos == searchViewModel.wordList.value?.size ?: return) pos = 0
        binding.vpWord.currentItem = pos
    }

    private fun onBackbuttonClick() {
        binding.clWords.removeAllViews()
        searchViewModel.setCurrent(-1)
        (activity as SearchActivity).back()
    }


    private fun addAll() {

        AlertDialog.Builder(activity!!)
                .setMessage(resources.getString(R.string.add_else))
                .setNegativeButton(resources.getString(R.string.cancel)) { d, _ -> d.dismiss() }
                .setPositiveButton(resources.getString(R.string.confirm)) { d, _ ->
                    val list = searchViewModel.wordList.value
                    if (list != null) {
                        wordViewModel.add(context!!, list)
                    } else {
                        Toast.makeText(context, getString(R.string.error), Toast.LENGTH_SHORT).show()
                    }
                    d.dismiss()
                }
                .show()
    }


}