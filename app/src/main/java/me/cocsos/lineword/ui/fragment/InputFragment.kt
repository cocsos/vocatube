package me.cocsos.lineword.ui.fragment

import android.app.Activity
import android.app.Activity.RESULT_OK
import android.content.Context.INPUT_METHOD_SERVICE
import android.content.Intent
import android.os.Bundle
import android.speech.RecognizerIntent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import me.cocsos.lineword.BuildConfig
import me.cocsos.lineword.R
import me.cocsos.lineword.databinding.FragmentInputBinding
import me.cocsos.lineword.ui.activity.SearchActivity
import me.cocsos.lineword.ui.activity.OcrCaptureActivity
import me.cocsos.lineword.viewmodel.SearchViewModel
import me.cocsos.lineword.viewmodel.WordViewModel
import org.koin.android.viewmodel.ext.android.sharedViewModel


class InputFragment : androidx.fragment.app.Fragment(), View.OnClickListener {

    private val TAG = this.javaClass.simpleName
    lateinit var binding: FragmentInputBinding

    val FROM_OCR_CAPTURE_ACTIVITY = 1010
    val FROM_VOICE_RECOGNITION = 1011

    lateinit var wordViewModel :WordViewModel
    val searchViewModel by sharedViewModel<SearchViewModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        super.onCreate(savedInstanceState)
        Log.e(TAG, "--InputFragment--")
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_input, container, false)
        binding.wvm = wordViewModel
        setButtonsAndSettings()
        binding.searchViewModel = searchViewModel
        binding.lifecycleOwner = this
        return binding.root
    }
    private fun setButtonsAndSettings() {

        //SEE test
        if (BuildConfig.DEBUG) {
            val test = resources.getString(R.string.example)
            searchViewModel.query = test
        }

        binding.tvOk.setOnClickListener(this)
        binding.ivCamera.setOnClickListener(this)
        binding.ivMic.setOnClickListener(this)
        binding.svScroll.overScrollMode = View.OVER_SCROLL_ALWAYS
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.iv_camera -> getPicture()
            R.id.iv_mic -> getAudio()
            R.id.tv_ok -> {
                val imm = context?.getSystemService(INPUT_METHOD_SERVICE) as? InputMethodManager
                imm?.hideSoftInputFromWindow(binding.etInput.windowToken, 0)
                (activity as SearchActivity).next()
            }
        }
    }

    private fun getAudio() {
//
        val i = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        i.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "en-US")
        try {
            startActivityForResult(i, FROM_VOICE_RECOGNITION)
        } catch (e: Exception) {
            Toast.makeText(context, "Error initializing speech to text engine.", Toast.LENGTH_LONG).show()
        }
    }


    private fun getPicture() {//사진을 받아오면 다시 호출

        val intent = Intent(context, OcrCaptureActivity::class.java)
        startActivityForResult(intent, FROM_OCR_CAPTURE_ACTIVITY)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d(TAG, "onActivityResult: from  = $requestCode, result = $resultCode")
        when (requestCode) {
            FROM_OCR_CAPTURE_ACTIVITY -> {
                if (resultCode == Activity.RESULT_OK) {
                    val text = data?.getStringExtra("text") ?: ""
                    Log.d(TAG, "onActivityResult: $text")
                    binding.etInput.setText(text)
                    binding.tvOk.performClick()
                }
            }//가져온 사진 보내기
            FROM_VOICE_RECOGNITION -> {
                if (resultCode == RESULT_OK) {
                    val thingsYouSaid = data?.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)

                    binding.etInput.setText(binding.etInput.text.toString() + thingsYouSaid)
                }
            }
        }
    }



    private fun translate() {

    }

}