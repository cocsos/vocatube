package me.cocsos.lineword.ui.adapter

import android.content.Context
import androidx.databinding.DataBindingUtil
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import me.cocsos.lineword.R
import me.cocsos.lineword.databinding.ViewWordBinding
import me.cocsos.lineword.model.Word
import me.cocsos.lineword.viewmodel.WordViewModel


class WordViewPagerAdapter(val context: Context, private val wordViewModel: WordViewModel) : androidx.viewpager.widget.PagerAdapter() {
    var items = listOf<Word>()

    override fun isViewFromObject(p0: View, p1: Any) = (p0 == p1 as View)
    override fun getCount() = items.size
    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val bind = DataBindingUtil.inflate<ViewWordBinding>(LayoutInflater.from(context), R.layout.view_word, null, false)
        bind.wvm = wordViewModel
        bind.word = items[position]

//        bind.ivAdd.setOnClickListener {
//            wordViewModel.addWord(it, items[position])
////            refreshAndNext(position)  //다음 페이지로 이동한다
//            bind.word = items[position]
//            bind.executePendingBindings()
//        }
//
//        bind.ivSpecial.setOnClickListener {
//            wordViewModel.onSpecialButtonClicked(it, items[position])
////            refreshAndNext(position)  //다음 페이지로 이동한다
//            bind.word = items[position]
//            bind.executePendingBindings()
//        }

        (container as ViewPager).addView(bind.root, 0)
        return bind.root
    }


    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        (container as ViewPager).removeView(`object` as View)
    }
}
