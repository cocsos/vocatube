package me.cocsos.lineword.ui.activity

import android.app.AlertDialog
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.View
import android.view.Window
import android.widget.RemoteViews
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.google.android.material.snackbar.Snackbar
import me.cocsos.lineword.BaseApplication
import me.cocsos.lineword.R
import me.cocsos.lineword.databinding.ActivitySearchBinding
import me.cocsos.lineword.databinding.DialogLineOptionBinding
import me.cocsos.lineword.ui.fragment.InputFragment
import me.cocsos.lineword.ui.fragment.ResultFragment
import me.cocsos.lineword.viewmodel.SearchViewModel
import me.cocsos.lineword.viewmodel.SettingViewModel
import me.cocsos.lineword.viewmodel.WordViewModel
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel


class SearchActivity : AppCompatActivity(), View.OnClickListener {

    private val TAG = this.javaClass.simpleName
    lateinit var binding: ActivitySearchBinding

    val FROM_COFFEE_VIEW = 1020

    val searchViewModel by viewModel<SearchViewModel>()

    val wordViewModel by inject<WordViewModel>()
    val settingViewModel by inject<SettingViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_search)

        binding.settingViewModel = settingViewModel
        binding.lifecycleOwner = this
        setViewPager()
    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.actionbar_actions, menu)
        return true
    }

    private fun setViewPager() {
        val adapter = SlidePagerAdapter(supportFragmentManager)

        binding.vpFragment.adapter = adapter
        binding.vpFragment.currentItem = 0
    }

    private fun setUpNotificationBar() {

        val cameraOnNoti = BaseApplication.sharedPreferences.getBoolean("camera_on_noti", false)
        val voiceOnNoti = BaseApplication.sharedPreferences.getBoolean("voice_on_noti", false)
        val screenshotOnNoti = BaseApplication.sharedPreferences.getBoolean("screenshot_on_noti", false)
        val dictOnNoti = BaseApplication.sharedPreferences.getBoolean("dict_on_noti", false)


        var flag = 0

        if (cameraOnNoti) flag += 0b0001
        if (voiceOnNoti) flag += 0b0010
        if (screenshotOnNoti) flag += 0b0100
        if (dictOnNoti) flag += 0b1000


        settingNotificationBar(flag)

    }

    private fun settingNotificationBar(flag: Int) {


//        if (!sharedPreferences.getBoolean("notification", true)) return
//
//        val titleIntent = Intent(this, BaseActivity::class.java)
//        val pendingIntent = PendingIntent.getActivity(this, 1, titleIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val notifyID = 1
        val CHANNEL_ID = "me.cocsos.oneshot.simple"
        val CHANNEL_NAME = "me.cocsos.oneshot"
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {   //오레오 이상일 경우
//            Log.d(TAG, "sendNotification:  version >= 26 O")
//
//            val screenIntent = Intent()
//            val pendingIntent = PendingIntent.getActivity(this, 1, screenIntent, PendingIntent.FLAG_UPDATE_CURRENT)
//
//            val action = Notification.Action(R.drawable.ic_screen, "Previous", pendingIntent)
////            val action = NotificationCompat.Action(R.drawable.ic_screen, title, pendingIntent)
//
//            val b = Notification.Builder(this, CHANNEL_ID)
//                    .setSmallIcon(R.drawable.ic_app)
//                    .setContentTitle("스피드 검색 >")
//                    .addAction(action)
//
////                    .addAction(R.drawable.iv_camera, "BUTTON 1", myIntentToButtonOneScreen) // #0
////                    .addAction(R.drawable.iv_mic, "BUTTON 2", myIntentToButtonTwoScreen)  // #1
//                    .setChannelId(CHANNEL_ID)
//                    .setOngoing(true)
//
//            val no = b.build()
//            val importance = NotificationManager.IMPORTANCE_HIGH
//
//            // Create a notification and set the notification channel.
//            val mNotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
//            val channel = NotificationChannel(CHANNEL_ID, "oneshot_notification", importance)
//
//            mNotificationManager.createNotificationChannel(channel)
//            mNotificationManager.notify(notifyID, no)
//
//        } else {
        Log.d(TAG, "sendNotification:  version < 26 O??")


        val speedOn = BaseApplication.sharedPreferences.getBoolean("speedSearch", false)

        if (speedOn) {

        }
        val screenIntent = Intent(this@SearchActivity, OcrCaptureActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(this, 1, screenIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

            val importance = NotificationManager.IMPORTANCE_HIGH

            val mChannel = NotificationChannel(
                    CHANNEL_ID, CHANNEL_NAME, importance)

            notificationManager.createNotificationChannel(mChannel)

        }


        val remoteView = RemoteViews(packageName, R.layout.view_noti)

//   PendingIntent pending = PendingIntent.getBroadcast(context, widgetId,
//       intent, 0);
        remoteView.setOnClickPendingIntent(R.id.iv_screen, pendingIntent)


        val no = NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_app2)
//                .setContentTitle("스피드 검색 >")
//                .setContentText("스피드 검색 >")
                .setContent(remoteView)
//                .addAction(R.drawable.ic_screen, "BUTTON 3", pendingIntent)
                .setChannelId(CHANNEL_ID)
                .setOngoing(true)
                .setVibrate(longArrayOf(0L))
                .build()
        notificationManager.notify(1 /* ID of notification */, no)
        Toast.makeText(applicationContext, "다시 버튼을 클릭하면 종료할 수 있습니다", Toast.LENGTH_SHORT).show()
//        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d(TAG, "onActivityResult: from  = $requestCode, result = $resultCode")
        when (requestCode) {
            FROM_COFFEE_VIEW -> {
                Snackbar.make(binding.clContainer, "Thank You So Much!", Snackbar.LENGTH_SHORT).show()
            }

        }
    }


//    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
//        when (item?.itemId) {
//            android.R.id.home -> finish()
//            R.id.action_coffee -> showCoffeeView()
//            R.id.action_quick -> showCoffeeView()
//            R.id.action_setting -> showSettingDialog()
//        }
//
//        return super.onOptionsItemSelected(item)
//    }

    private fun showCoffeeView() {
        val url = "https://www.buymeacoffee.com/COCSOS"

        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        startActivityForResult(intent, FROM_COFFEE_VIEW)
    }

     fun showSettingDialog() {
        val bind = DataBindingUtil.inflate<DialogLineOptionBinding>(layoutInflater, R.layout.dialog_line_option, null, false)
        val optionDialog = AlertDialog.Builder(this@SearchActivity)
                .setView(bind.root)
                .create()

        optionDialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
        optionDialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        optionDialog.show()

        var show_memorize = BaseApplication.sharedPreferences.getBoolean("show_memorize", true)
        var show_special = BaseApplication.sharedPreferences.getBoolean("show_special", true)
        var show_wrong = BaseApplication.sharedPreferences.getBoolean("show_wrong", true)

        bind.swMemorize.isChecked = show_memorize
        bind.swSpecial.isChecked = show_special
        bind.swWrong.isChecked = show_wrong


        bind.swMemorize.setOnClickListener {
            show_memorize = !show_memorize
            BaseApplication.sharedPreferences.edit().putBoolean("show_memorize", show_memorize).apply()
            BaseApplication.sharedPreferences.edit().putBoolean("show_special", show_special).apply()
            BaseApplication.sharedPreferences.edit().putBoolean("show_wrong", show_wrong).apply()
        }

        bind.swSpecial.setOnClickListener {
            show_special = !show_special
            BaseApplication.sharedPreferences.edit().putBoolean("show_special", show_special).apply()
        }

        bind.swWrong.setOnClickListener {
            show_wrong = !show_wrong
            BaseApplication.sharedPreferences.edit().putBoolean("show_wrong", show_wrong).apply()
        }

        optionDialog.setCancelable(true)
        optionDialog.setOnDismissListener {
            Toast.makeText(applicationContext, "다음 검색부터 적용됩니다", Toast.LENGTH_SHORT).show()
            window?.decorView?.findViewById<View>(android.R.id.content)?.invalidate()
        }

    }


    /**
     * 입력창과 결과창을 이동하는 PagerAdapter
     */
    private inner class SlidePagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
        override fun getCount(): Int = 2
        val fragments: List<Fragment> =
                listOf(InputFragment().apply {
                    wordViewModel = this@SearchActivity.wordViewModel
                }, ResultFragment().apply {
                    wordViewModel = this@SearchActivity.wordViewModel
                }
                )

        override fun getItem(position: Int): Fragment {


            val frag: Fragment = fragments[position]

            return frag
        }
    }


    fun back() {

        when (binding.vpFragment.currentItem) {
            1 -> binding.vpFragment.currentItem = 0
        }

    }

    fun next() {
        when (binding.vpFragment.currentItem) {
            0 -> {
                binding.vpFragment.currentItem = 1
                val adapter = binding.vpFragment.adapter as SlidePagerAdapter
                (adapter.fragments[1] as ResultFragment).startAnalyse()
            }
        }

    }

}