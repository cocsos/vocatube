package me.cocsos.lineword.ui.activity

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.view.inputmethod.EditorInfo
import android.widget.AdapterView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import com.google.android.material.snackbar.Snackbar
import me.cocsos.lineword.BuildConfig
import me.cocsos.lineword.R
import me.cocsos.lineword.databinding.ActivityMainBinding
import me.cocsos.lineword.databinding.DialogFilterWordBinding
import me.cocsos.lineword.ui.adapter.WordAdapter
import me.cocsos.lineword.util.SwipeHelper
import me.cocsos.lineword.viewmodel.DictationViewModel2
import me.cocsos.lineword.viewmodel.MainViewModel
import me.cocsos.lineword.viewmodel.SettingViewModel
import me.cocsos.lineword.viewmodel.WordViewModel
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel


class MainActivity : AppCompatActivity(), View.OnClickListener {

    private val TAG = this.javaClass.simpleName
    lateinit var binding: ActivityMainBinding

    private val wordViewModel by inject<WordViewModel>()
    private val settingViewModel by inject<SettingViewModel>()
    private val dictationViewModel by inject<DictationViewModel2>()

    private val mainViewModel by viewModel<MainViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        supportActionBar?.hide()
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.wordViewModel = wordViewModel
        binding.settingViewModel = settingViewModel
        binding.dictationViewModel = dictationViewModel
        binding.mainViewModel = mainViewModel

        binding.lifecycleOwner = this
        setButtons()
        setSwiper()

    }


    private fun updateWordResult(str: String) {
        val w = dictationViewModel.makeWord(str)
        if (w.mean == "no data") {
            mainViewModel.setWord(null)
            Snackbar.make(binding.etQuestion, getString(R.string.no_data), Snackbar.LENGTH_SHORT).show()
        } else {
            mainViewModel.setWord(w)
        }

    }

    private fun setButtons() {
        binding.ivFilter.setOnClickListener(this)
        binding.ivSearchAll.setOnClickListener(this)


        binding.etQuestion.setOnEditorActionListener { v, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                val str = (v as TextView).text.toString()
                updateWordResult(str)
                binding.etQuestion.dismissDropDown()
                return@setOnEditorActionListener true
            }
            return@setOnEditorActionListener false
        }
        binding.etQuestion.onItemClickListener = object : AdapterView.OnItemClickListener {
            override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                Log.d(TAG, "onItemClick: pos =$position")
                val adapter = parent?.adapter ?: return
                val str = adapter.getItem(position) as? String ?: return
                updateWordResult(str)
                binding.etQuestion.dismissDropDown()
            }
        }

        binding.etQuestion.addTextChangedListener(
                object : TextWatcher {
                    override fun afterTextChanged(s: Editable?) {
                        val s = s?.toString()
                        if (TextUtils.isEmpty(s)) mainViewModel.setWord(null)
                    }

                    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

                    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
                })
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.iv_filter -> showFilterDialog()
            R.id.iv_search_all -> openSearchActivity()
//            R.id.fab -> fabButtonClicked()
//            R.id.fab1 -> openSearchActivity()
//            R.id.fab2 -> openWordLibarary()
        }
    }

    private fun openSearchActivity() {
        val intent = Intent(applicationContext, SearchActivity::class.java)
        startActivity(intent)
    }


    private fun showFilterDialog() {
        val bind = DataBindingUtil.inflate<DialogFilterWordBinding>(layoutInflater, R.layout.dialog_filter_word, null, false)
        val optionDialog = AlertDialog.Builder(this@MainActivity)
                .setView(bind.root)
                .create()

        optionDialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
        optionDialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        optionDialog.show()

        bind.ovm = settingViewModel

        optionDialog.setCancelable(true)
        optionDialog.setOnDismissListener {
            binding.settingViewModel = settingViewModel
            binding.executePendingBindings()
        }
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            android.R.id.home -> finish()
        }
        return true
    }


    private fun setSwiper() {

        val swipeHelper = object : SwipeHelper(this, binding.rvWord) {
            override fun instantiateUnderlayButton(viewHolder: androidx.recyclerview.widget.RecyclerView.ViewHolder?, underlayButtons: MutableList<UnderlayButton>?) {
                underlayButtons?.add(UnderlayButton(
                        applicationContext,
                        UnderlayButtonClickListener {

                            viewHolder?.adapterPosition?.let {
                                if (it == -1) {
                                    binding.rvWord.adapter?.notifyDataSetChanged()
                                    Log.e(TAG, "instantiateUnderlayButton: position -1")
                                    return@UnderlayButtonClickListener
                                }
                            }

                            AlertDialog.Builder(this@MainActivity)
                                    .setMessage(resources.getString(R.string.ask_remove))
                                    .setPositiveButton(resources.getString(R.string.confirm)) { dialog, _ ->
                                        viewHolder?.adapterPosition?.let { pos -> (binding.rvWord.adapter as WordAdapter).remove(pos) }
                                        dialog.dismiss()
                                    }
                                    .setNegativeButton(resources.getString(R.string.cancel)) { d, _ -> d.dismiss() }

                                    .show()

                        }
                ))
            }
        }
    }


}
