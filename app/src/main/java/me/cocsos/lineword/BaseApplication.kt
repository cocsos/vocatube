package me.cocsos.lineword

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import me.cocsos.lineword.viewmodel.*
import org.koin.android.ext.android.startKoin
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.Module
import org.koin.dsl.module.module


class BaseApplication : Application() {

    internal var TAG = BaseApplication::class.java.simpleName

    private val PREF_NAME = "OneshotWord"

    override fun onCreate() {
        super.onCreate()

        sharedPreferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)

//        gson = GsonBuilder().registerTypeAdapterFactory(ItemTypeAdapterFactory()).create()
        setModules()
        gson = Gson()

    }


    private fun setModules() {
        val retrofitModule: Module = module(override = true) {
            viewModel { SearchViewModel() }
            viewModel { MainViewModel() }

            single {
                DictationViewModel2().apply {
                    loadWord(applicationContext.resources)
                }
            }
            single {
                SettingViewModel().apply {
                    loadOption(applicationContext)
                }
            }
            single {
                WordViewModel().apply {
                    loadWords(applicationContext)
                }
            }
            viewModel {
                //검색에서만 필요한 뷰모델
                SearchViewModel()
            }

        }

        val appModules = listOf(retrofitModule)
        startKoin(this, appModules)
    }

    companion object {

        lateinit var gson: Gson

        lateinit var sharedPreferences: SharedPreferences

    }


}
