package me.cocsos.lineword.util

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query


interface RetrofitClient {
    //리스트정보를 가져오는 getYtbList
    //SEE 쿼터 게산기 https://developers.google.com/youtube/v3/determine_quota_cost
    //SEE https://www.googleapis.com/youtube/v3/videos?id=sgdPlDG1-8k&key=AIzaSyBDyHKYlA0sfUGzdMpkgZ0GsCa9l8s05YE&fields=items(id,snippet(channelId,title,categoryId),statistics,contentDetails(duration,caption))&part=snippet,statistics,contentDetails
//    @GET("videos/")
//    fun getYtbList(@Query("id") id: String, @Query("key") key: String,
//                   @Query("fields") fields: String, @Query("part") part: String): Observable<List<YtbItem>>


    //https://www.googleapis.com/youtube/v3/subscriptions?
    // mine=true&key=AIzaSyBDyHKYlA0sfUGzdMpkgZ0GsCa9l8s05YE&fields=items(id,snippet(channelId,title,categoryId),contentDetails(duration,caption))&part=snippet,contentDetails


    //    public String ytbListFields = "items(id,snippet(channelId,title,categoryId),statistics,contentDetails(duration,caption))";@GET("youtube/v3/videos/")
    //https://www.googleapis.com/youtube/v3/captions/id


    // https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes
    @GET("captions/{id}") //성공하든 못하든, (자막이 있든없든 쿼터는 소모된다. 200! 확인 후 호출할 것.)
    fun getSubtitle(@Query("id") id: String): Observable<String>

    //    @FormUrlEncoded
    //    @POST("users/token")
    //    public Observable<Boolean> insertToken(@Field("personStr") String personStr);


    //#############################getPopularList 지역(regionCode)과 관심주제(videoCategoryId)의 유명한 동영상을 가져온다.
    // 190104  입력 파라메터 예시 ex)
    // chart=mostPopular
    // &key=AIzaSyBDyHKYlA0sfUGzdMpkgZ0GsCa9l8s05YE
    // X &fields=items(id,snippet(channelId,title,categoryId),statistics,contentDetails(duration,caption)),nextPageToken SEE statistics는 빠진다
    // O &fields=items(id,snippet(channelId,title,categoryId),contentDetails(duration,caption)),nextPageToken
    // &part=snippet,statistics,contentDetails
    // &maxResults=10
    // &pageToken=CAoQAA
    //    String popularListField = "items(id,snippet(channelId,title,categoryId),statistics,contentDetails(duration,caption)),nextPageToken";
    //    String popularListPart = "snippet,statistics,contentDetails";
    //    @GET("videos/") //성공하든 못하든, (자막이 있든없든 쿼터는 소모된다. 200! 확인 후 호출할 것.)
    //    Observable<String> getPopularList(@Query("chart") String chart,
    //                                          @Query("key") String key,
    //                                          @Query("fields") String fields,
    //                                          @Query("part") String part,
    //                                          @Query("maxResult") int maxResult,
    //                                          @Query("pageToken") String pageToken);

//https://www.googleapis.com/youtube/v3/captions?part=snippet&videoId=HyFvp5uO9as&fields=items&key=AIzaSyBDinf_OmSkm8QVGzwvjHP_phicJ7FDuAE  는 잘 됨
    //https://www.googleapis.com/youtube/v3/captions?part=snippet&videoId=xCIJJ2Rb368&fields=items&key=AIzaSyBDinf_OmSkm8QVGzwvjHP_phicJ7FDuAE 는 에러!?
    @GET("/youtube/v3/captions")
    fun checkCaption(@Query("videoId") videoId: String, @Query("key") key: String, @Query("part") snippet: String="snippet", @Query("fields") items: String="items"):Observable<String>

    @GET("/youtube/v3/search")
    fun searchQuery(@Query("q") videoId: String, @Query("key") key: String, @Query("pageToken") pageToken:String ="", @Query("part") snippet: String="snippet", @Query("maxResults") maxResults : Int = 20):Observable<String>


@Headers (  "Accept-Encoding: gzip, deflate")
    @GET(".")
    fun loadCaptionLink(@Query("url") url: String):Observable<String>


    //Oauth를 성공해야함.
//https://www.googleapis.com/youtube/v3/captions/id?id=캡션트랙ID
    @GET("captions/id")
    fun setCaption(@Query("id") id: String): Observable<String>

    //SEE https://www.googleapis.com/youtube/v3/videos?id=sgdPlDG1-8k&key=AIzaSyBDinf_OmSkm8QVGzwvjHP_phicJ7FDuAE&fields=items(id,snippet(channelId,title,categoryId),statistics,contentDetails(duration,caption))&part=snippet,statistics,contentDetails


    /**
     *
     * 파파고 NMT API
     * curl "https://openapi.naver.com/v1/papago/n2mt" \
    -H "Content-Type: application/x-www-form-urlencoded; charset=UTF-8" \
    -H "X-Naver-Client-Id: kMEbnz_I35vcMx7iz_4K" \
    -H "X-Naver-Client-Secret: Wr1ooykSUl" \
    -d "source=ko&target=en&text=만나서 반갑습니다." -v
     *
     */


    /**
     *
     * 파파고 SMP API
     * curl "https://openapi.naver.com/v1/language/translate" \
    -H "Content-Type: application/x-www-form-urlencoded; charset=UTF-8" \
    -H "X-Naver-Client-Id: kMEbnz_I35vcMx7iz_4K" \
    -H "X-Naver-Client-Secret: Wr1ooykSUl" -v \
    -d "source=ko&target=en&text=만나서 반갑습니다." -v
     *
     */
}
