package me.cocsos.lineword.util;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import android.content.Context;

import me.cocsos.lineword.model.Word;

@Database(entities = { Word.class }, version = 1, exportSchema = false)
public abstract class RepoDatabase extends RoomDatabase {

    private static RepoDatabase INSTANCE;

    public static RepoDatabase getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE =  Room.databaseBuilder(context.getApplicationContext(),RepoDatabase .class, "user-database")
                            .fallbackToDestructiveMigration() //버젼 업 할때 이전 DB를 버린다!
                            .build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }
    public abstract WordDao getWordDao();
}