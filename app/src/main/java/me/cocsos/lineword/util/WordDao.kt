package me.cocsos.lineword.util

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

import io.reactivex.Single
import me.cocsos.lineword.model.Word

@Dao
interface WordDao {
    @get:Query("SELECT * FROM Word")
    val words: Single<List<Word>>

    @get:Query("SELECT * FROM Word")
    val wordsNow: List<Word>


    @Query("SELECT * FROM Word WHERE Word.origin ==:origin")
    fun getWord(origin: String): Single<Word>

    @Query("SELECT * FROM Word WHERE Word.origin ==:origin")
    fun getWordNow(origin: String): Word?

    @Query("DELETE FROM Word WHERE Word.origin ==:origin")
    fun delete(origin: String): Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(word: Word): Long?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(list: List<Word>)

}
